package models

import (
	"database/sql"
	"log"
	"strconv"
)

func ToModels(sqlRows *sql.Rows) []TodoItem {
	var list []TodoItem
	defer func(result *sql.Rows) {
		err := result.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(sqlRows)
	for sqlRows.Next() {
		var item TodoItem
		err := sqlRows.Scan(&item.ID, &item.TEXT)
		if err != nil {
			log.Fatal(err)
		}
		list = append(list, item)
	}
	return list
}
func ToModel(sqlResult *sql.Rows) TodoItem {
	var list TodoItem
	defer func(result *sql.Rows) {
		err := result.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(sqlResult)
	for sqlResult.Next() {
		err := sqlResult.Scan(&list.ID, &list.TEXT)
		if err != nil {
			log.Fatal(err)
		}
	}
	return list
}
func ToId(sqlResult *sql.Rows) string {
	var id int
	defer func(result *sql.Rows) {
		err := result.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(sqlResult)
	for sqlResult.Next() {
		err := sqlResult.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
	}
	return strconv.Itoa(id)
}
