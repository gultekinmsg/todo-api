package models

type TodoItem struct {
	ID   int    `json:"id"`
	TEXT string `json:"text"`
}
type Todo struct {
	TODO string `json:"todo"`
}
type Response struct {
	ITEM TodoItem `json:"item"`
	MSG  string   `json:"message"`
}
type ListResponse struct {
	ITEMS []TodoItem `json:"items"`
	MSG   string     `json:"message"`
}
