package handlers

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"todo-api/mocks"
	"todo-api/models"
)

func TestRepository_GetAll_Successful(t *testing.T) {
	mockRepoController := gomock.NewController(t)
	mockRepo := mocks.NewMockTodoRepository(mockRepoController)
	mockRepoController.Finish()
	app := createTestApp()
	NewHandler(mockRepo).RegisterRoutes(app)
	mockRepo.EXPECT().GetAll().Return(getAllMockData(), nil)
	req := newHTTPRequestWithJSON(http.MethodGet, "/api/todo", http.NoBody)
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)
}
func TestRepository_GetAll_Db_Failed(t *testing.T) {
	mockRepoController := gomock.NewController(t)
	mockRepo := mocks.NewMockTodoRepository(mockRepoController)
	mockRepoController.Finish()
	app := createTestApp()
	NewHandler(mockRepo).RegisterRoutes(app)
	mockRepo.EXPECT().GetAll().Return(getAllMockData(), errors.New("db fail"))
	req := newHTTPRequestWithJSON(http.MethodGet, "/api/todo", http.NoBody)
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusServiceUnavailable, res.StatusCode)
}
func TestRepository_Create_Successful(t *testing.T) {
	mockRepoController := gomock.NewController(t)
	mockRepo := mocks.NewMockTodoRepository(mockRepoController)
	mockRepoController.Finish()
	app := createTestApp()
	NewHandler(mockRepo).RegisterRoutes(app)
	mockRepo.EXPECT().Create(models.Todo{TODO: "test"}).Return(createMockData(), nil)
	req := newHTTPRequestWithJSON(http.MethodPost, "/api/todo", fiber.Map{"todo": "test"})
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusCreated, res.StatusCode)
}
func TestRepository_Create_Failed_Parsing_Body(t *testing.T) {
	mockRepoController := gomock.NewController(t)
	mockRepo := mocks.NewMockTodoRepository(mockRepoController)
	mockRepoController.Finish()
	app := createTestApp()
	NewHandler(mockRepo).RegisterRoutes(app)
	mockRepo.EXPECT().Create(models.Todo{TODO: "test"}).Return(createMockData(), nil)
	req := newHTTPRequestWithJSON(http.MethodPost, "/api/todo", 6)
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusUnprocessableEntity, res.StatusCode)
}
func TestRepository_Create_Empty_Request_Body_Value(t *testing.T) {
	mockRepoController := gomock.NewController(t)
	mockRepo := mocks.NewMockTodoRepository(mockRepoController)
	mockRepoController.Finish()
	app := createTestApp()
	NewHandler(mockRepo).RegisterRoutes(app)
	mockRepo.EXPECT().Create(models.Todo{TODO: "test"}).Return(createMockData(), nil)
	req := newHTTPRequestWithJSON(http.MethodPost, "/api/todo", fiber.Map{"todo": ""})
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
}
func TestRepository_Create_Db_Failed(t *testing.T) {
	mockRepoController := gomock.NewController(t)
	mockRepo := mocks.NewMockTodoRepository(mockRepoController)
	mockRepoController.Finish()
	app := createTestApp()
	NewHandler(mockRepo).RegisterRoutes(app)
	mockRepo.EXPECT().Create(models.Todo{TODO: "test"}).Return(createMockData(), errors.New("db fail"))
	req := newHTTPRequestWithJSON(http.MethodPost, "/api/todo", fiber.Map{"todo": "test"})
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusServiceUnavailable, res.StatusCode)
}
func getAllMockData() []models.TodoItem {
	var list []models.TodoItem
	for i := 0; i < 5; i++ {
		var item models.TodoItem
		item.ID = i
		item.TEXT = "todo item"
		list = append(list, item)
	}
	return list
}
func createMockData() (todo models.TodoItem) {
	return models.TodoItem{
		ID:   1,
		TEXT: "test todo",
	}
}
func createTestApp() *fiber.App {
	return fiber.New(fiber.Config{})
}
func newHTTPRequestWithJSON(method, url string, v interface{}) *http.Request {
	body, _ := json.Marshal(v)
	req := httptest.NewRequest(method, url, bytes.NewReader(body))
	req.Header.Add("Content-type", "application/json")
	return req
}
