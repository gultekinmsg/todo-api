package handlers

import (
	"github.com/gofiber/fiber/v2"
	"log"
	"net/http"
	"todo-api/models"
	"todo-api/repository"
)

type TodoHandler interface {
	RegisterRoutes(app *fiber.App)
	GetAll(c *fiber.Ctx) error
	Create(c *fiber.Ctx) error
}
type Handler struct {
	repository repository.TodoRepository
}

func NewHandler(repo repository.TodoRepository) *Handler {
	return &Handler{
		repository: repo,
	}
}
func (h Handler) RegisterRoutes(app *fiber.App) {
	app.Get("/api/todo", h.GetAll)
	app.Post("/api/todo", h.Create)
}

func (h Handler) GetAll(c *fiber.Ctx) error {
	all, err := h.repository.GetAll()
	if err != nil {
		log.Println("dependency failed")
		c.Status(http.StatusServiceUnavailable)
		return nil
	}
	var response models.ListResponse
	response.MSG = "Success"
	response.ITEMS = all
	c.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	err = c.Status(http.StatusOK).JSON(response)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}
func (h Handler) Create(c *fiber.Ctx) error {
	var request models.Todo
	if err := c.BodyParser(&request); err != nil {
		log.Println("can not parse request body")
		c.Status(http.StatusUnprocessableEntity)
		return nil
	}
	if request.TODO == "" {
		log.Println("request data is empty")
		c.Status(http.StatusBadRequest)
		return nil
	}
	create, err := h.repository.Create(request)
	if err != nil {
		log.Println("dependency failed")
		c.Status(http.StatusServiceUnavailable)
		return nil
	}
	var response models.Response
	response.MSG = "Success"
	response.ITEM = create
	c.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	err = c.Status(http.StatusCreated).JSON(response)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}
