package server

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"todo-api/handlers"
)

type Server struct {
	app  *fiber.App
	port string
}

func NewServer(port string, handlers []handlers.TodoHandler) Server {
	app := fiber.New()
	app.Use(cors.New(cors.Config{AllowOrigins: "*", AllowMethods: "GET,POST,HEAD,PUT,DELETE,PATCH,OPTIONS", AllowHeaders: "*"}))
	server := Server{app: app, port: port}
	server.addRoutes()
	for _, handler := range handlers {
		handler.RegisterRoutes(app)
	}
	return server
}

func (s Server) Run() {
	err := s.app.Listen(s.port)
	if err != nil {
		panic(err.Error())
	}
}

func (s Server) addRoutes() {
	s.app.Get("/api/health", healthCheck)
}
func healthCheck(c *fiber.Ctx) error {
	c.Status(fiber.StatusOK)
	return nil
}
