package server

import (
	"fmt"
	"github.com/phayes/freeport"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
	"todo-api/handlers"
)

func TestServer_Successful(t *testing.T) {
	freePort, err := freeport.GetFreePort()
	assert.Nil(t, err)
	port := fmt.Sprintf(":%d", freePort)
	s := NewServer(port, []handlers.TodoHandler{})
	go s.Run()
	testEndpointURL := fmt.Sprintf("http://localhost%s/api/health", port)
	req, err := http.NewRequest(http.MethodGet, testEndpointURL, http.NoBody)
	assert.Nil(t, err)
	resp, err := http.DefaultClient.Do(req)
	assert.Nil(t, err)
	defer resp.Body.Close()
	assert.Equal(t, http.StatusOK, resp.StatusCode)
}
func TestServer_Fail_Invalid_Port(t *testing.T) {
	invalidPort := ":-1"
	s := NewServer(invalidPort, []handlers.TodoHandler{})
	assert.Panics(t, func() { s.Run() })
}
