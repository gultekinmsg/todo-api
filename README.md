

# Todo Backend

This application has two capabilities;
1. Create Todo Item (`POST /api/todo`): Stores todo item with given text
2. Get All Todo items (`GET /api/todo`): Retrieves all stored todo items

To send request to backend at staging or production:

**Staging url:** http://128.199.32.244:8080  
**Production url:** http://206.189.14.14:8080

## Technologies and Tools Used
- Go as programming language
- Fiber 2 as web framework
- Postgresql as database
- Golang/mock for mocks
- Pact-go for contract testing
- Docker
- Docker-compose for deployment
- Gitlab CI for CI/CD tasks
- Wait for fixing docker-compose services start order (https://github.com/ufoscout/docker-compose-wait)

## Architecture
- We have two different servers, first one is `staging`, second one is `production`
- `staging` server is used for test purposes, acceptance tests are also running on this server.
- Inside each server, we have a docker-compose deployment.
- Compose file contains 3 different services.
    - `todoapp-api` Backend
    - `todoapp-web` Frontend
    - `todoapp-db` Postgresql database
- To deploy new version of application(api or web), ci/cd enters into server (staging and production)
    - Removes current containers(`docker-compose down`)
    - Pulls new image (`docker-compose pull`)
    - Starts containers (`docker-compose up -d`)
- see `.gitlab-ci.yml` for all stage and details of pipeline/

## Development
To run backend for at local:
1. Enter to project's root directory and create a postgresql database with docker-compose :
> docker-compose up -d

**This will give you postgresql db up and running.*
2. Run application from terminal:
> make install

> make run

**Your backend url will be localhost:8080*

## Endpoints
### Get Todos
Returns all todo items
```
curl --location --request GET '128.199.32.244:8080/api/todo'
```

**Sample Response:**
```json
{
	"items": [
		{
			"id": 1,
			"text": "task to do"
		}
	],
	"message": "Success"
}
```

### Create Todo
Creates todo item with given text
```
curl --location --request POST '128.199.32.244:8080/api/todo' \
--header 'Content-Type: application/json' \
--data-raw '{
"todo": "task to do"
}'
```
**Sample Response:**
```json
{
	"item": {
		"id": 64,
		"text": "task to do"
	},
	"message": "Success"
}
```
## Deployment to Your Environment
You can create a docker-compose file and start your todo app with `docker-compose up -d` command

Sample docker-compose file;
```yml  
version: '3.4' services:    
  todoapp-db:    
    image: postgres:14.1    
    environment:    
      - POSTGRES_DB=todoapp    
      - POSTGRES_USER=todousr    
      - POSTGRES_PASSWORD=strongpassword    
    volumes:    
      - postgres:/var/lib/postgresql/data    
    networks:    
      - todonet    
  todoapp-api:    
    image: msgmsg/todo-backend:stable    
    ports:    
      - 8080:8080    
    environment:    
      - "POSTGRES_URL=host=todoapp-db port=5432 user=todousr password=strongpassword dbname=todoapp sslmode=disable"    
  - "PORT=:8080"    
  - "WAIT_HOSTS=todoapp-db:5432"    
  networks:    
      - todonet    
  todoapp-web:    
    image: msgmsg/todo-frontend:production    
    ports:    
      - 8081:8081    
    environment:    
      - "WAIT_HOSTS=todoapp-api:8080"    
  networks:    
- todonet volumes:    
postgres: networks:    
  todonet:    
    driver: bridge  
```  
## Links
- Frontend repository: https://gitlab.com/muhammedgultekin/todo
- Acceptance test repository: https://gitlab.com/muhammedgultekin/todo-acceptance
## Contributors
Muhammed Said Gültekin (gultekinmsg@gmail.com)