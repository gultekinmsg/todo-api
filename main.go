package main

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	"todo-api/handlers"
	"todo-api/repository"
	"todo-api/server"
)

func main() {
	if err := RunApp(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
}

func RunApp() error {
	err := godotenv.Load(".env.local")
	if err != nil {
		log.Println(".env.local not found, reading parameters from os env variables")
	}

	credentials := os.Getenv("POSTGRES_URL")
	port := os.Getenv("PORT")
	pqDb, err := repository.NewPostgresDBConnection(credentials)
	if err != nil {
		log.Fatal(err)
	}
	repo := repository.NewRepository(pqDb)
	handler := handlers.NewHandler(repo)
	serverHandler := []handlers.TodoHandler{
		handler,
	}
	s := server.NewServer(port, serverHandler)
	s.Run()
	return nil
}
