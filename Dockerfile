FROM golang:1.17-alpine
ENV GOOS linux
ENV CGO_ENABLED 0
WORKDIR /app
COPY . .
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait
RUN go mod download
RUN go build -o app
CMD /wait && ./app
