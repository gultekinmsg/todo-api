package cdc

import (
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"log"
	"os"
	"testing"
	"todo-api/handlers"
	"todo-api/mocks"
	"todo-api/models"
	"todo-api/server"
)

type settings struct {
	Consumer                   string
	Provider                   string
	DisableToolValidityCheck   bool
	ProviderBaseURL            string
	PactURLs                   []string
	BrokerURL                  string
	BrokerToken                string
	PublishVerificationResults bool
	ProviderVersion            string
	Tags                       []string
}

func (s *settings) create() {
	s.Consumer = "todo-ui"
	s.Provider = "todo-api"
	s.DisableToolValidityCheck = true
	s.ProviderBaseURL = "http://localhost:8081"
	s.PactURLs = []string{os.Getenv("PACT_BROKER_HOST") + "/pacts/provider/todo-api/consumer/todo-ui/latest/master"}
	s.BrokerURL = os.Getenv("PACT_BROKER_HOST")
	s.BrokerToken = os.Getenv("PACT_BROKER_API_TOKEN")
	s.PublishVerificationResults = true
	s.ProviderVersion = os.Getenv("CI_COMMIT_SHORT_SHA")
	s.Tags = []string{"master"}
}

func TestPactBroker(t *testing.T) {
	var todoRepo *mocks.MockTodoRepository
	controller := gomock.NewController(t)
	defer controller.Finish()
	todoRepo = startServerWithMocks(controller)

	setting := settings{}
	setting.create()
	pact := dsl.Pact{
		Consumer:                 setting.Consumer,
		Provider:                 setting.Provider,
		DisableToolValidityCheck: setting.DisableToolValidityCheck,
	}

	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            setting.ProviderBaseURL,
		PactURLs:                   setting.PactURLs,
		BrokerURL:                  setting.BrokerURL,
		Tags:                       setting.Tags,
		BrokerToken:                setting.BrokerToken,
		PublishVerificationResults: setting.PublishVerificationResults,
		ProviderVersion:            setting.ProviderVersion,
		StateHandlers: types.StateHandlers{
			"There are todo items": func() error {
				todoRepo.EXPECT().GetAll().Return(getAllMockData(), nil)
				return nil
			},
			"Todo item created": func() error {
				todoRepo.EXPECT().Create(models.Todo{TODO: "task to do"}).Return(createMockData(), nil)
				return nil
			},
		},
		PactLogLevel: "DEBUG",
	})
	if err != nil {
		log.Fatal(err)
	}
}
func getAllMockData() []models.TodoItem {
	var list []models.TodoItem
	for i := 1; i < 11; i++ {
		var item models.TodoItem
		item.ID = i
		item.TEXT = fmt.Sprintf("task to do %v", i)
		list = append(list, item)
	}
	return list
}
func createMockData() models.TodoItem {
	return models.TodoItem{
		ID:   11,
		TEXT: "task to do 11",
	}
}

func startServerWithMocks(controller *gomock.Controller) *mocks.MockTodoRepository {
	port := ":8081"
	mockRepo := mocks.NewMockTodoRepository(controller)

	mockHandler := handlers.NewHandler(mockRepo)
	serverHandler := []handlers.TodoHandler{
		mockHandler,
	}
	s := server.NewServer(port, serverHandler)
	go s.Run()
	return mockRepo
}
