package repository

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
	"strconv"
	"todo-api/models"
)

type TodoRepository interface {
	GetAll() (todos []models.TodoItem, err error)
	Create(request models.Todo) (todo models.TodoItem, err error)
}

type Repository struct {
	pqSql *sql.DB
}

func NewRepository(db *sql.DB) *Repository {
	return &Repository{pqSql: db}
}

func (s *Repository) GetAll() (todos []models.TodoItem, err error) {
	db := s.pqSql
	sqlStatement := `SELECT "ID","TEXT" FROM todos.todo order by "ID" desc;`
	rows, err := db.Query(sqlStatement)
	if err != nil {
		log.Fatal(err)
	}
	responseItems := models.ToModels(rows)
	return responseItems, err
}
func (s *Repository) Create(request models.Todo) (todo models.TodoItem, err error) {
	db := s.pqSql
	sqlStatement := `INSERT INTO todos.todo("TEXT") VALUES($1) RETURNING "ID";`
	rows, err := db.Query(sqlStatement, request.TODO)
	if err != nil {
		log.Fatal(err)
	}

	sqlStatement2 := `SELECT "ID","TEXT" FROM todos.todo WHERE "ID" = $1;`
	new, err := strconv.Atoi(models.ToId(rows))
	if err != nil {
		log.Fatal(err)
	}
	row, err := db.Query(sqlStatement2, new)
	if err != nil {
		log.Fatal(err)
	}
	createdRow := models.ToModel(row)

	return createdRow, err
}
