package repository

import (
	"database/sql"
	"log"
)

func NewPostgresDBConnection(credentials string) (*sql.DB, error) {
	db, err := sql.Open("postgres", credentials)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	todoSchema := `CREATE SCHEMA IF NOT EXISTS todos;`
	_, err = db.Exec(todoSchema)
	if err != nil {
		log.Fatal(err)
	}
	todoTable := `CREATE TABLE IF NOT EXISTS todos.todo (
			"ID"       	SERIAL PRIMARY KEY NOT NULL,
			"TEXT"   		TEXT NOT NULL
		);`
	_, err = db.Exec(todoTable)
	return db, err
}
