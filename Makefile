build:
	go build main.go
run:
	go run ./main.go
unit-test:
	go test ./... -short
generate-mocks:
	mockgen -source handlers/handler.go -package mocks -destination mocks/mock_handler.go
	mockgen -source repository/repository.go -package mocks -destination mocks/mock_repository.go
install:
	go mod tidy
lint:
	golangci-lint run