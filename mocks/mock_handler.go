// Code generated by MockGen. DO NOT EDIT.
// Source: handlers/handler.go

// Package mocks is a generated GoMock package.
package mocks

import (
	reflect "reflect"

	fiber "github.com/gofiber/fiber/v2"
	gomock "github.com/golang/mock/gomock"
)

// MockTodoHandler is a mock of TodoHandler interface.
type MockTodoHandler struct {
	ctrl     *gomock.Controller
	recorder *MockTodoHandlerMockRecorder
}

// MockTodoHandlerMockRecorder is the mock recorder for MockTodoHandler.
type MockTodoHandlerMockRecorder struct {
	mock *MockTodoHandler
}

// NewMockTodoHandler creates a new mock instance.
func NewMockTodoHandler(ctrl *gomock.Controller) *MockTodoHandler {
	mock := &MockTodoHandler{ctrl: ctrl}
	mock.recorder = &MockTodoHandlerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockTodoHandler) EXPECT() *MockTodoHandlerMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockTodoHandler) Create(c *fiber.Ctx) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", c)
	ret0, _ := ret[0].(error)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockTodoHandlerMockRecorder) Create(c interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockTodoHandler)(nil).Create), c)
}

// GetAll mocks base method.
func (m *MockTodoHandler) GetAll(c *fiber.Ctx) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAll", c)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetAll indicates an expected call of GetAll.
func (mr *MockTodoHandlerMockRecorder) GetAll(c interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAll", reflect.TypeOf((*MockTodoHandler)(nil).GetAll), c)
}

// RegisterRoutes mocks base method.
func (m *MockTodoHandler) RegisterRoutes(app *fiber.App) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "RegisterRoutes", app)
}

// RegisterRoutes indicates an expected call of RegisterRoutes.
func (mr *MockTodoHandlerMockRecorder) RegisterRoutes(app interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RegisterRoutes", reflect.TypeOf((*MockTodoHandler)(nil).RegisterRoutes), app)
}
